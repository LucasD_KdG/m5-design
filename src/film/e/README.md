Het hangt er van af of gebruikers tegelijk programmamanager en filmmanager kunnen zijn,
en of een programmanager een filmmanager kan worden.
Als een van beide waar is is model c waarschijnlijk het beste.
Als dat niet zo is, is c nog steeds OK, maar er is dan wel één van de collecties (films/voorstellingen) die niet gebruikt wordt.
Hoe meer niet gedeelde attributen/relaties fiml- en contentbeheerder hebben, hoe interessanter model a is.
In dat geval, én als een gebruiker meerdere rollen heeft of vaak van rol wisselt, wordt model d interessant.