package film.d;

public abstract class Role {
    private RoleOwner owner;

    public Role(RoleOwner owner) {
        this.owner = owner;
    }
}
